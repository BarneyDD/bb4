<?php
//  Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
  $messages = array();
  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
      // Если есть параметр save, то выводим сообщение пользователю.
      ?><script> alert('Спасибо, данные были сохраненны.');</script>
      <?php
   
  }

// Складываем признак ошибок в массив.
$errors = array();
$errors['fio'] = !empty($_COOKIE['fio_error']);
$errors['email'] = !empty($_COOKIE['email_error']);
$errors['date'] = !empty($_COOKIE['date_error']);
$errors['sex'] = !empty($_COOKIE['sex_error']);
$errors['edu'] = !empty($_COOKIE['edu_error']);
$errors['course'] = !empty($_COOKIE['course_error']);
$errors['comment'] = !empty($_COOKIE['comment_error']);
$errors['check'] = !empty($_COOKIE['check_error']);

// Выдаем сообщения об ошибках.
if ($errors['fio']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('fio_error', '', 100000);
  // Выводим сообщение.
 if ($errors['fio'] == 'empty') $messages['fio'] = '<div class="errors">Name error.</div>';
 else $messages['fio'] = '<div class="errors">Only letters and white space allowed.</div>';
}
if ($errors['email']) {
    setcookie('email_error', '', 100000);
   if($errors['email'] == 'empty') $messages['email'] = '<div class="errors">Email is required.</div>';
   else $messages['email'] = '<div class="errors">Invalid email format.</div>';
  }
if ($errors['date']) {
    setcookie('date_error', '', 100000);
   $messages['date'] = '<div class="errors">Date is required.</div>';
  }
if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
   $messages['sex'] = '<div class="errors">Pol is required.</div>';
  }
if ($errors['edu']) {
    setcookie('edu_error', '', 100000);
   $messages['edu'] = '<div class="errors">Konechenost is required.</div>';
  }
if ($errors['course']) {
    setcookie('course_error', '', 100000);
   $messages['course'] = '<div class="errors">Super_sposobnosti is required.</div>';
  }
if ($errors['comment']) {
    setcookie('comment_error', '', 100000);
   $messages['comment'] = '<div class="errors">Biografi is required.</div>';
  }
if ($errors['check']) {
    setcookie('check_error', '', 100000);
   $messages['check'] = '<div class="errors">Soglasie is required.</div>';
  }

// Складываем предыдущие значения полей в массив, если есть.
$values = array();
$values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['edu'] = empty($_COOKIE['edu_value']) ? '' : $_COOKIE['edu_value'];
$values['course'] = empty($_COOKIE['course_value']) ? '' : $_COOKIE['course_value'];
$values['comment'] = empty($_COOKIE['comment_value']) ? '' : $_COOKIE['comment_value'];
$values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else{
$errors = FALSE;

if (empty($_POST['fio'])) {
    // Выдаем куку до конца сессии с флажком об ошибке в поле fio.
    setcookie('fio_error', 'empty');
  $errors = TRUE;
}
else { if (!preg_match('/^[A-Za-zа-яА-Я\s]+$/iu',$_POST['fio'])) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
       }
    }

if (empty($_POST['email'])) {
    setcookie('email_error', 'empty');
    $errors = TRUE;
}
else { if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
       }
     }

if (empty($_POST['date'])) {
    setcookie('date_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['sex']){
    setcookie('sex_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['edu']){
    setcookie('edu_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('edu_value', $_POST['edu'], time() + 365 * 24 * 60 * 60);
  }


if (empty($_POST['course'])) {
    setcookie('course_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('course_value', serialize($_POST['course']), time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['comment']){
    setcookie('comment_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('comment_value', $_POST['comment'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['check']){
    setcookie('check_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('check_value', $_POST['check'], time() + 365 * 24 * 60 * 60);
  }



if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('edu_error', '', 100000);
    setcookie('course_error', '', 100000);
    setcookie('comment_error', '', 100000);
    setcookie('check_error', '', 100000);
   
  }

  $ability1 = in_array('Бессмертие', $_POST['course']) ? 1 : 0;
  $ability2 = in_array('Прохождение сквозь стены', $_POST['course']) ? 1 : 0;
  $ability3 = in_array('Левитация', $_POST['course']) ? 1 : 0;
  $ability4 = in_array('Fly', $_POST['course']) ? 1 : 0;

  // Сохранение в базу данных 
  $user = 'u20400';
  $pass = '4723206';
  $db = new PDO('mysql:host=localhost;dbname=u20400', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  
  //Подготовленный запрос
  //неименованные метки
  
  try {$stmt = $db->prepare("INSERT INTO application SET fio = ?, email = ?, date = ?, sex = ?, education = ?, Бесмертие = ?, Прохождение сквозь стены = ?, Левитация = ?, Fly = ?, comment = ?");
  $stmt->execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$_POST['sex'],$_POST['edu'],$ability1,$ability2,$ability3,$ability4,$_POST['comment']));
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
