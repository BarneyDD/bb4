<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1">
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <style>
body{
    width: 100%;
    z-index: -10;
    background-color:#B3B0EE;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
* {box-sizing: border-box;}
.transparent {
  position: relative;
  max-width: 600px;
  padding: 50px 50px;
  margin: 30px auto 0;
  background-color: rgba(49,83,63,0.5);
  background-size: cover;
}
.errors {
  color: red;
}
.form input[type="submit"] {background: rgb(69, 93, 92);}
.error {
  border: 2px solid red;
}
form{
    text-align: center;
}
</style>
    <div class="cover-block"> 
<form class="transparent" action="" method="POST">
<div class="form">
<h3>Forma</h3>
<label>
       <p>Введите имя: </p> 
   <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="Ваня Иванов">
   <?php if (!empty($messages['fio'])) {print($messages['fio']);} ?>
   </label>
  <label>
       <p>Введите e-mail: </p>
   <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email" placeholder="test@example.com"> 
   <?php if (!empty($messages['email'])) {print($messages['email']);} ?>
      </label>
  <label>
      <p>Введите дату рождения: </p>
  <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="date" min="1920-01-01" max="2005-01-01">
  <?php if (!empty($messages['date'])) {print($messages['date']);} ?>
      </label>
     <label>
      <p>Выберите пол:<br>
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'woman'){print 'checked';} ?>" type="radio" value="woman"> Девушка</label> 
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'man'){print 'checked';} ?>" type="radio" value="man"> Мужчина</label></br></p>
      <?php if (!empty($messages['sex'])) {print($messages['sex']);} ?>
 </label> 
 <label>
      <p>Количество конечностей: <br>
	  <label class="radio"><input name="edu" type="radio" value="1" checked="<?php if($values['edu'] == '1') {print 'checked';} ?>"> 1</label>
      <label class="radio"><input name="edu" type="radio" value="2" checked="<?php if($values['edu'] == '2') {print 'checked';} ?>"> 2</label>
      <label class="radio"><input name="edu" type="radio" value="3" checked="<?php if($values['edu'] == '3') {print 'checked';} ?>"> 3</label>
      <label class="radio"><input name="edu" type="radio" value="4" checked="<?php if($values['edu'] == '4') {print 'checked';} ?>"> 4</label>
      <label class="radio"><input name="edu" type="radio" value="5" checked="<?php if($values['edu'] == '5') {print 'checked';} ?>"> 5</label><br></p>
      <?php if (!empty($messages['edu'])) {print($messages['edu']);} ?>
 </label>
 <label>
      <p>Выберите супер способности:<</p>
  <select name="course[]" multiple="multiple" <?php if ($errors['check']) {print 'class="error"';} ?>>
 <option value="Бессмертие" <?php if(in_array('Бессмертие',unserialize($values['course']))) {print 'selected';} ?> >Бессмертие</option>
 <option value="Прохождение сквозь стены" <?php if(in_array('Прохождение сквозь стены',unserialize($values['course']))) {print 'selected';} ?>>Прохождение сквозь стены</option>
 <option value="Левитация" <?php if(in_array('Левитация',unserialize($values['course']))) {print 'selected';} ?>>Левитация</option>
 <option value="Fly" <?php if(in_array('Fly',unserialize($values['course']))) {print 'selected';} ?>>Fly </option>
  </select>
</label>
<?php if (!empty($messages['course'])) {print($messages['course']);} ?>
   <label>
      <p>Комментарий:</p>
		<textarea name="comment" <?php if ($errors['comment']) {print 'class="error"';} ?> rows="3" cols="40"><?php print $values['comment']; ?></textarea>
          </label>
          <?php if (!empty($messages['comment'])) {print($messages['comment']);} ?>
  <label>
      <p>С контрактом ознакомлен/a</p>
		<input name="check" <?php if ($errors['check']) {print 'class="error"';} ?> checked="<?php if($values['check']) print 'checked'; ?>" type="checkbox" id="check"> Я согласен/а.
        </label>
        <?php if (!empty($messages['check'])) {print($messages['check']);} ?>
  
  <p> <input type="submit" value="SEND" /></p>
  </div>
</form>
</div>
</body>
</html>
